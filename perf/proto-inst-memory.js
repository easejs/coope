/**
 * Outputs the amount of time to run various tests against a prototype
 * implementation of Foo
 */

function Foo( name )
{
    this.name = name;
}

Foo.prototype = {
    getName: function()
    {
        return this.name;
    },

    setName: function( newname )
    {
        this.name = newname;
    },
};

console.log(
    require( './inst-call-cmp' ).run( Foo ).join( '\t' )
);
