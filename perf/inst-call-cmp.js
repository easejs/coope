/**
 * Performance testing on an instance of Foo (rather specific, I know.
 * Lazy-programmer-who-wants-to-get-back-to-writing-his-article syndrome).
 */

exports.run = function( Foo )
{
    // get initial memory usage and start time in ms
    var mem   = process.memoryUsage().heapUsed,
        start = ( new Date() ).getTime();

    // instantiate a number of Foo's, keeping them in memory.
    var i = count = 1e6, a = [];
    while ( i-- )
    {
        a[i] = new Foo();
    }

    // gather final statistics before doing any additional work
    var end     = ( ( new Date() ).getTime() - start ),
        mem_end = ( process.memoryUsage().heapUsed - mem ),
        cstart  = ( new Date() ).getTime();

    i = count;
    while ( i-- )
    {
        a[0].getName();
        a[0].setName();
    };

    var cend = ( ( new Date() ).getTime() - cstart );

    return [ mem_end, end, cend ];
}
