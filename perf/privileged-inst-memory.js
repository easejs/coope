/**
 * Outputs the amount of time to run various tests against a protected member
 * implementation of Foo
 */

function Foo( name )
{
    this.getName = function()
    {
        return name;
    };

    this.setName = function( newname )
    {
        name = newname;
    };
}

console.log(
    require( './inst-call-cmp' ).run( Foo ).join( '\t' )
);
