# This file is under the public domain.

sec = coope.tex \
	coope.bib \
	sec/class-like.tex \
	sec/encap-hacks.tex \
	sec/hacking-proto.tex \
	sec/licenses.tex

src = coope.tex
aux = coope.aux

.PHONY: default pdf dvi clean

default: pdf

pdf: $(sec)
	pdflatex -shell-escape $(src)
	bibtex $(aux)
	pdflatex -shell-escape $(src)
	pdflatex -shell-escape $(src)

dvi: $(sec)
	latex -shell-escape $(src)
	bibtex $(aux)
	latex -shell-escape $(src)
	latex -shell-escape $(src)

clean:
	rm -f coope.{pdf,dvi}
