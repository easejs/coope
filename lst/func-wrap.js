function wrap( func )
{
    return function()
    {
        return 'one ' +
            func.apply( this, arguments ) +
            ' three';
    };
}

function str( value )
{
    return value;
}

wrap( str )( 'two' );  // "one two three"


// demonstrating wrappers with prototypes
function Foo( value )
{
    this._value = value;
};

Foo.prototype = {
    bar: wrap( function()
    {
        return this._value;
    } )
};

var inst = new Foo( '2' );
inst.bar();  // "one 2 three"
