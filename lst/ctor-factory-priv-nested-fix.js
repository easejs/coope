    function wrap( method )
    {
        return function()
        {
            var prev = this.__priv;

            // expose private member object
            this.__priv =
                this.constructor[ _privname ];

            var retval = method.apply(
                this, arguments
            );

            // restore previous value
            this.__priv = prev;
            return retval;
        };
    }
