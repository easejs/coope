function Foo()
{
    this.Boolean = true;
}

// ok
var inst = new Foo();
inst.Boolean; // true

// bad
Foo();
new Boolean(); // TypeError
