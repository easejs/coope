var Database = Class(
{
    __construct: function( host, user, pass )
    {
        // __priv contains private members
        var ip = this.__priv._resolveHost();

        // __priv is undefined
        this.__priv._connect( ip, user, pass );
    },

    // ...
} );
